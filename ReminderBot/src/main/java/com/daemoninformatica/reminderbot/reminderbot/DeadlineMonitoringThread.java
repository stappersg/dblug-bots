/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.reminderbot;

import com.daemoninformatica.reminderbot.data.Reminder;
import java.util.List;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author martin
 */
public class DeadlineMonitoringThread implements Runnable
{
    private final List<Reminder>    reminders;
    private final PircBot           bot;
    private final String            channel;
    
    public DeadlineMonitoringThread(final PircBot bot, final String channel, 
            final List<Reminder> reminders) 
    {
        this.bot        = bot;
        this.reminders  = reminders;
        this.channel    = channel;
    }

    private void evaluateReminder()
    {
        if(reminders.isEmpty())
        {
            // System.out.println("No reminders in store. Just ending cycle..");
            return;
        }
        
        // System.out.println("I actually have a message to evaluate...");
        
        // Evaluate deadline of first object. 
        Reminder r = reminders.get(0);
        
        // if deadline has passed: 
        if(r.deadlineHasPassed())
        {
            System.out.println("Processing reminder with passed deadline: " 
                    + r);
            
            // send reminder message to user. 
            this.bot.sendMessage(this.channel, "@" + r.getForNick()+ ": " + 
                    r.getMessage());
            
            // remove object from the list. 
            this.reminders.remove(0);
        }
    }
    
    @Override
    public void run() 
    {
        while(true)
        {
            evaluateReminder();
            
            // Thread.yield();
            try
            {
                Thread.sleep(1000);
            }
            catch(InterruptedException e)
            {
                System.err.println("DeadlineMonitoringTrhead received an "
                        + "interrupt!");
            }
        }
    }
}
