/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.reminderbot;

import com.daemoninformatica.reminderbot.data.exceptions.ReminderBotException;

/**
 *
 * @author martin
 */
public class ReminderBotMain 
{
    private static final String BOT_NAME        = "rememberal";
    private static final int    IRC_SERVER_PORT = 6667;
    private static final String IRC_SERVER_NAME = "irc.freenode.net";
    private static final String CHANNEL_NAME    = "#dblug";
    
    
    public void start()
    {
        ReminderBot bot1 = new ReminderBot(BOT_NAME, IRC_SERVER_NAME, 
                IRC_SERVER_PORT, CHANNEL_NAME);
        
        try
        {
            bot1.start();
            while(true)
                Thread.yield();
        }
        catch(ReminderBotException rb_e)
        {
            System.err.println("Failed to start Reminderbot: " + 
                    rb_e.getMessage());
        }
    }
    
    public static void main(String args[])
    {
        System.out.println("ReminderBot V0.1, written by DaemonInformatica.");
        
        ReminderBotMain bot = new ReminderBotMain();
        bot.start();
    }
}
