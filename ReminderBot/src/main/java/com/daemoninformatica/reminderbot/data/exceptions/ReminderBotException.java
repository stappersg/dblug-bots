/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.data.exceptions;

/**
 *
 * @author martin
 */
public class ReminderBotException extends Exception
{
    public ReminderBotException(String string) {
        super(string);
    }

    public ReminderBotException(String string, Throwable thrwbl) 
    {
        super(string, thrwbl);
    }
}
