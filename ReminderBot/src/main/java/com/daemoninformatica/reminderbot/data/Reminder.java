/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daemoninformatica.reminderbot.data;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 *
 * @author martin
 */
public class Reminder 
{
    private final String        forNick;
    private final LocalDateTime deadline;
    private final String        message;

    public Reminder(String forNick, LocalDateTime deadLine, final String message) 
    {
        this.forNick    = forNick;
        this.deadline   = deadLine;
        this.message    = message;
    }

    public String getForNick() 
    {
        return forNick;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public String getMessage() 
    {
        return message;
    }

    
    public boolean deadlineHasPassed()
    {
        // https://docs.oracle.com/javase/8/docs/api/java/time/ZoneId.html : 
        // ECT - Europe/Paris
        return deadline.isBefore(LocalDateTime.now(ZoneId.of("CET")));
    }

    private String padInteger(final int value)
    {
        String output = value < 10 ? "0" : "";
        output += value;
        
        return output;
    }
    
    private String getDeadlineString()
    {
        String output = "";
        output += deadline.getYear();
        output += "-";
        output += padInteger(deadline.getMonthValue());
        output += "-";
        output += deadline.getDayOfMonth();
        output += " ";
        output += padInteger(deadline.getHour());
        output += ":";
        output += padInteger(deadline.getMinute());
        output += ":";
        output += padInteger(deadline.getSecond());
        
        return output;
    }
    
    @Override
    public String toString() 
    {
        return "Deadline: " + getDeadlineString() + " | nick: " + forNick + 
                " | message: " + message;
    }
}
